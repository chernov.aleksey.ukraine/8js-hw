const input = document.getElementById( 'input' ); 
const message = document.createElement( "p" );
const message2 = document.createElement( "button" );
message2.innerText = 'X';
message2.style.cssText = `
padding-left: 4px;
height: 18px;
width: 18px;
background-color: #c7c1c1;
border-radius: 12px;
border: .1px solid #696969;
color: #696969;
`;
input.onfocus = () => {
	input.style.borderColor = 'green';
	input.style.color = 'black';
}
input.onblur = function ()  {
	input.style.borderColor = '#696969';
	if (input.value < 0 || isNaN(input.value)|| input.value === "-0"|| input.value.includes(' ')) {
		input.style.borderColor = 'red';
		message.innerHTML = '<strong>Please enter correct price</strong>';
		input.after(message);
	} else if (input.value === "0"  || input.value === '' ) {
		message.remove();
		message2.remove();
		input.value = '';
	} else {
		message.remove();
		input.style.borderColor = '#696969';
		input.style.color = 'green';
		message.innerHTML = 'Текущая цена: ' + input.value +'     ';
		input.before(message);
		message.append(message2);
	}
};
message2.addEventListener('click', () => {
	input.style.borderColor = '#696969';
	input.style.color = 'black';
	message.remove();
	message2.remove();
	input.value = '';
});

/*Опишите своими словами, как Вы понимаете, что такое обработчик событий.

Функция, которая сработает, как только событие произошло. */



